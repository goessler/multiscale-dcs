(** ocamldec.ml is generated automatically *) 

open Toolkit 
open Ode 
open Symbolic 
open Lts 
open Safety
open Control
open Reachability
open Plot 



let dim = 2 

let mds = [|"m_1"; "m_2"|] 

let r0 = 1.
let vs = 1.
let rl = 0.05
let rc = 0.005
let xl = 3.
let xc = 70.
let xd = 7. *. 2. -. 3. *. log (3.)


let a1_val_0_0 = -. (rl) /. xl
let a1_0_0 (t:float) = a1_val_0_0

let a1_val_0_1 = 0.
let a1_0_1 (t:float) = a1_val_0_1

let a1_val_1_0 = 0.
let a1_1_0 (t:float) = a1_val_1_0

let a1_val_1_1 = -1. /. (xc *. (r0 +. rc))
let a1_1_1 (t:float) = a1_val_1_1

let a1 = [|[|a1_0_0 ; a1_0_1|]; [|a1_1_0 ; a1_1_1|]|]

let a2_val_0_0 = -. ((rl +. ((r0 *. rc) /. (r0 +. rc)))) /. xl
let a2_0_0 (t:float) = a2_val_0_0

let a2_val_0_1 = -. ((r0 /. (r0 +. rc))) /. (5. *. xl)
let a2_0_1 (t:float) = a2_val_0_1

let a2_val_1_0 = (5. *. r0 /. (r0 +. rc)) /. xc
let a2_1_0 (t:float) = a2_val_1_0

let a2_val_1_1 = -. ((1. /. xc *. (r0 +. rc)))
let a2_1_1 (t:float) = a2_val_1_1

let a2 = [|[|a2_0_0 ; a2_0_1|]; [|a2_1_0 ; a2_1_1|]|]

let a3_val_0_0 = -0.5
let a3_0_0 (t:float) = a3_val_0_0

let a3_val_0_1 = 0.
let a3_0_1 (t:float) = a3_val_0_1

let a3_val_1_0 = 0.
let a3_1_0 (t:float) = a3_val_1_0

let a3_val_1_1 = -0.005
let a3_1_1 (t:float) = a3_val_1_1

let a3 = [|[|a3_0_0 ; a3_0_1|]; [|a3_1_0 ; a3_1_1|]|]

let a4_val_0_0 = -0.4
let a4_0_0 (t:float) = a4_val_0_0

let a4_val_0_1 = 0.
let a4_0_1 (t:float) = a4_val_0_1

let a4_val_1_0 = 0.
let a4_1_0 (t:float) = a4_val_1_0

let a4_val_1_1 = -0.004
let a4_1_1 (t:float) = a4_val_1_1

let a4 = [|[|a4_0_0 ; a4_0_1|]; [|a4_1_0 ; a4_1_1|]|]

let a5_val_0_0 = -0.3
let a5_0_0 (t:float) = a5_val_0_0

let a5_val_0_1 = 0.
let a5_0_1 (t:float) = a5_val_0_1

let a5_val_1_0 = 0.
let a5_1_0 (t:float) = a5_val_1_0

let a5_val_1_1 = -0.003
let a5_1_1 (t:float) = a5_val_1_1

let a5 = [|[|a5_0_0 ; a5_0_1|]; [|a5_1_0 ; a5_1_1|]|]

let a6_val_0_0 = -0.2
let a6_0_0 (t:float) = a6_val_0_0

let a6_val_0_1 = 0.
let a6_0_1 (t:float) = a6_val_0_1

let a6_val_1_0 = 0.
let a6_1_0 (t:float) = a6_val_1_0

let a6_val_1_1 = -0.002
let a6_1_1 (t:float) = a6_val_1_1

let a6 = [|[|a6_0_0 ; a6_0_1|]; [|a6_1_0 ; a6_1_1|]|]

let b_val_0 = vs /. xl
let b_0 (t:float) = b_val_0

let b_val_1 = pi
let b_1 (t:float) = b_val_1

let b = [|b_0 ; b_1|]

let d_val_0 = logB 2. 10. +. log (3.) -. xc *. 3.
let d_0 (t:float) = d_val_0

let d_1 (t:float) = exp (3.) *. t
let d = [|d_0 ; d_1|]

let e (t:float) = sin (pi) +. cos (pi /. 2.) +. tan (3. *. pi /. 2.) *. t +. (1.0 /.  tan (5.)) +. (1.0 /. cos (3.))

let f_val_0_0 = asin (3. +. tan (2.))
let f_0_0 (t:float) = f_val_0_0

let f_val_0_1 = acos (5. +. atan (3. *. pi))
let f_0_1 (t:float) = f_val_0_1

let f_val_1_0 = sinh (2.) +. cosh (3. *. pi)
let f_1_0 (t:float) = f_val_1_0

let f_val_1_1 = tanh (3. *. sin (pi) +. sqrt (10.))
let f_1_1 (t:float) = f_val_1_1

let f = [|[|f_0_0 ; f_0_1|]; [|f_1_0 ; f_1_1|]|]

let a_val = (2. ** 3.) +. exp (3.)
let a (t:float) = a_val

let safe0 = 
  Sp ([|{lower = 1.3000 ; upper = 1.7000}; {lower = 5.7000 ; upper = 5.8000}|])

let safe1 = 
  Sp ([|{lower = 1.3000 ; upper = 1.7000}; {lower = 5.7500 ; upper = 5.8000}|])

let safe2 = 
  Sp ([|{lower = 1.3000 ; upper = 1.7000}; {lower = 5.7000 ; upper = 5.7500}|])

let safe3 = 
  Sp ([|{lower = 1.7500 ; upper = 1.9000}; {lower = 5.9000 ; upper = 6.0000}|])

let safe4 = 
  SpMinusSp ([|{lower = -6.0000 ; upper = 6.0000}; {lower = -4.0000 ; upper = 4.0000}|], [|{lower = -1.5000 ; upper = 1.5000}; {lower = -1.0000 ; upper = 1.0000}|])

let init4 = 
  SpMinusSp ([|{lower = -6.0000 ; upper = 6.0000}; {lower = -4.0000 ; upper = 4.0000}|], [|{lower = -1.5000 ; upper = 1.5000}; {lower = -1.0000 ; upper = 1.0000}|])

let target4 = 
  Sp ([|{lower = 2.0000 ; upper = 5.5000}; {lower = 2.0000 ; upper = 3.5000}|])

let init0 = 
  Sp ([|{lower = 1.3000 ; upper = 1.7000}; {lower = 5.7000 ; upper = 5.8000}|])

let target = 
  Sp ([|{lower = 1.5000 ; upper = 1.6000}; {lower = 5.7000 ; upper = 5.7250}|])

(* the vector fields for x *)

let f_x_0_0_0 (x:float_vector) (t:float) = 
  cos (x.(0)) +. cos (x.(1)) *. t

let f_x_0_0_1 (x:float_vector) (t:float) = 
  a t +. cos (x.(1)) +. log (x.(0))

let f_x_2_1_0 (x:float_vector) (t:float) = 
  sin (x.(0)) +. cos (x.(1)) *. t

let f_x_2_1_1 (x:float_vector) (t:float) = 
  a t +. cos (x.(1)) +. log (x.(0))

let f_x_0 (t:float) (x:float_vector) = 
 if in_simple_space 2 x safe1 then 
  get_float_vector_from_tfunvec t [|f_x_0_0_0 x; f_x_0_0_1 x|]
 else if in_simple_space 2 x safe3 then 
  (vector_float_plus (matrix_vector_float_prod (get_float_matrix_from_tfunmat t a3) x) (get_float_vector_from_tfunvec t b))
 else 
  (vector_float_plus (matrix_vector_float_prod (get_float_matrix_from_tfunmat t a5) x) (get_float_vector_from_tfunvec t b))

let f_x_1 (t:float) (x:float_vector) = 
 if in_simple_space 2 x safe1 then 
  (vector_float_plus (matrix_vector_float_prod (get_float_matrix_from_tfunmat t a1) x) (get_float_vector_from_tfunvec t b))
 else if in_simple_space 2 x safe3 then 
  (vector_float_plus (matrix_vector_float_prod (get_float_matrix_from_tfunmat t a4) x) (get_float_vector_from_tfunvec t b))
 else 
  get_float_vector_from_tfunvec t [|f_x_2_1_0 x; f_x_2_1_1 x|]

let vfs_x () = 
 let res = H.create 0 in
 H.add res 0 f_x_0;
 H.add res 1 f_x_1;
 res

let vfs () = 
 let res = H.create 0 in 
 H.add res "x" (vfs_x ());
 res

let tau = 0.5

let eta = 1. /. (40. *. sqrt (2.))

let fscale = 2

let sdwell = 1

let lat = {
 dim = dim;
 eta = eta;
 scales = fscale;
 safe = safe4;
 initial = init4;
 target = Obj.magic ()
}

let sys = H.find (vfs ()) "x" 

let synthesis () = 
 let symbo = initialize_symbolic tau lat mds (sdwell = -1) in
 let stime = Sys.time () in
 lazy_safety_synthesis symbo sys 0.000000 sdwell 4;
 let etime = Sys.time () in
 Printf.printf "Ended in %f seconds\n" (etime -. stime);
 Printf.printf "The abstraction size is %d states\n" (Lts.size symbo.lts);
 Printf.printf "The controllability ratio is %.2f\n" (Lts.control_ratio symbo.lts symbo.lat symbo.finer (sdwell = -1));
 Printf.printf "Proportion of transitions of duration %.3f is %.2f\n" ((2.0 ** (-0.0)) *. tau) (Lts.proportion symbo.lts 0);
 Printf.printf "Proportion of transitions of duration %.3f is %.2f\n" ((2.0 ** (-1.0)) *. tau) (Lts.proportion symbo.lts 1);
 Printf.printf "Proportion of transitions of duration %.3f is %.2f\n" ((2.0 ** (-2.0)) *. tau) (Lts.proportion symbo.lts 2);
 plot_2d_dots symbo 20.000 40.000 [3; 0] true [|"red"; "blue"|] [|"black"; "gray"; "white"|] (sdwell = -1) (aindex "m_1" symbo.modes)
