(** **********************************************************************************)
(** CoSyMA: a tool for COntroller SYnthesis using Multi-scale Abstractions           *)   
(** @author: Sebti Mouelhi                                                           *)
(**                                                                                  *)
(** Copyright (c) 2011-2015, INRIA Rh�ne-Alpes                                       *)
(** All rights reserved                                                              *)
(** **********************************************************************************)


open Toolkit
open Typecheck


let string_of_var_type = function
  | VT_real -> "real" 
  | VT_integer -> "int"
  | VT_interval (x,y) -> spf "[%f .. %f]" x y
  | VT_realtoreal -> "real -> real"
  | VT_realtointerval (x,y) -> spf "real -> [%f .. %f]" x y 
  | VT_matrix d -> spf "M(%d)" d
  | VT_vector d -> spf "V(%d)" d


(* alias for [string_of_var_type] *)
let sovt = string_of_var_type
  
(* Print a [var_type] to [stdout] *)
let print_var_type t = 
  pl (string_of_var_type t)
   
  
let string_of_binop = function
  | BOP_plus -> "Plus"
  | BOP_minus -> "Minus"
  | BOP_times -> "Times"
  | BOP_divide -> "Divide"
  | BOP_modulus -> "Modulus"
  | BOP_power -> "Power"
  | BOP_logarithm -> "Logarithm"
  | BOP_equals -> "Equals"

let symbol_of_binop = function
  | BOP_plus -> "+"
  | BOP_minus -> "-"
  | BOP_times -> "*"
  | BOP_divide -> "/"
  | BOP_modulus -> "%"
  | BOP_power -> "^"
  | BOP_logarithm -> "log"
  | BOP_equals -> "="



let string_of_unop = function 
  | UOP_plus -> "Unary Plus"
  | UOP_minus -> "Unary Minus"
  | UOP_sine -> "Sine"
  | UOP_cosine -> "Cosine"
  | UOP_tangent -> "Tangent"
  | UOP_cotangent -> "Cotangent"
  | UOP_secant -> "Secant"
  | UOP_cosecant -> "Cosecant"
  | UOP_arcsine -> "Arcsine"
  | UOP_arccosine -> "Arccosine"
  | UOP_arctangent -> "Arctangent"
  | UOP_hypsine -> "Hyperbolic Sine"
  | UOP_hypcosine -> "Hyperbolic Cosine"
  | UOP_hyptangent -> "Hyperbolic Tangent"
  | UOP_squareroot -> "Squareroot"
  | UOP_natlogarithm -> "Natlogarithm"
  | UOP_exponential -> "Exponential"

let symbol_of_unop = function 
  | UOP_plus -> "+"
  | UOP_minus -> "-"
  | UOP_sine -> "sin"
  | UOP_cosine -> "cos"
  | UOP_tangent -> "tg"
  | UOP_cotangent -> "cotg"
  | UOP_secant -> "sec"
  | UOP_cosecant -> "csc"
  | UOP_arcsine -> "asin"
  | UOP_arccosine -> "acos"
  | UOP_arctangent -> "atg"
  | UOP_hypsine -> "sinh"
  | UOP_hypcosine -> "cosh"
  | UOP_hyptangent -> "tanh"
  | UOP_squareroot -> "sqrt"
  | UOP_natlogarithm -> "ln"
  | UOP_exponential -> "exp"

let string_of_id = function Id s -> s | _ -> failwith "parsing_err:string_of_id"
let string_of_enumf = function Enum_field s -> s | _ -> failwith "parsing_err:string_of_enumf"

(** Print the parse ast to [stdout] *)

let print ?(prefix="") t = 
  let rec print_rec nb_space prf t = 
    let succ n = n + 2 in
    let make_spaces n = ps prf ; ps (repeat_pattern " " n) in
    make_spaces nb_space ;
    let ms_succ n = ps prf ; ps (repeat_pattern " " (succ n)) in
    let ms_wp n = ps (repeat_pattern " " n) in
    let rec_call t = print_rec (succ nb_space) prefix t in
    let box x = 
      let sp () =  make_spaces (succ nb_space) in
      sp(); pl "["; List.iter rec_call x; sp() ; pl "]" 
      in
    let subox x = 
      let sp () =  make_spaces (succ nb_space) in
      let sp2 () = ps (repeat_pattern " " 1) in
      sp2 (); pl ""; List.iter rec_call x; sp() ; pl "" 
      in
      match t with
        | Root (id,dim,mds,cts,vars,trjs,cfs,vfs,sps,spas,synth,plot) -> 
          ps "Switched " ; pl id ;  
          rec_call dim;
          rec_call mds;
          rec_call cts;
          rec_call vars;
          rec_call trjs;
          rec_call cfs;
          rec_call vfs; 
          rec_call sps;
          box spas;
          rec_call synth;
          rec_call plot
        | Dimension d -> pl "Dimension"; rec_call d; 
        | Modes mds -> pl "Modes"; box mds
        | Enum_field s -> ms_wp nb_space ; ps "Enumerated field: "; pl s
        | Constants cs -> pl "Constants"; box cs
        | Constants_by_value (lc,v) -> subox (List.map (fun c -> Constant (c,v)) lc)
        | Constant (n,v) -> pl "Constant"; rec_call n; rec_call v ; 
        | Variables vs -> pl "Variables: "; box vs
        | Variables_by_type (vs,tp) -> subox (List.map (fun v -> Variable (v,tp)) vs)
        | Variable (n,tp) -> pl "Variable"; rec_call n; ms_succ nb_space ; print_var_type tp
        | Trajectories (tgs) -> pl "Trajectories"; box tgs 
        | Trajectory (n,cps) -> pl "Trajectory"; rec_call n; box cps
        | No_coefficients -> pl "No declared linear coefficients !"
        | Coefficients (decs,vals) -> pl "Coefficients" ; box decs ; box vals
        | Coefficients_by_type (cs,tp) -> subox (List.map (fun c -> Coefficient (c,tp)) cs)
        | Coefficient (n,tp) -> pl "Coefficient"; rec_call n; ms_succ nb_space ; print_var_type tp
        | Coefficient_value (id,v) -> pl "Coefficient value"; rec_call id ; rec_call v
        | Matrix vl -> pl "Matrix"; box vl
        | Vector v -> pl "Vector"; box v
        | Vector_fields vfss -> pl "Vector fields"; box vfss
        | Vector_field_for (traj,vfs) -> pl "Vector fields for"; rec_call traj; rec_call vfs
        | Cases_by_spaces (cases) -> pl "Cases by spaces"; box cases
        | Case_by_space (spc,uc) -> pl "Case by space"; rec_call spc; rec_call uc
        | Unique_case (vfs) -> pl "Unique case"; box vfs
        | Simplified_vecfield (m,var,vl) -> pl "Simplified vector field"; rec_call m; rec_call var ; rec_call vl
        | Expanded_vecfields (m,lv) -> pl "Expanded vector fields"; rec_call m; box lv
        | Expanded_vecfield (var,vl) -> pl "  Expanded vector field"; rec_call var ; rec_call vl
        | Sampling_params sps -> pl "Sampling parameters"; box sps
        | Time_sampling tau -> pl "Time sampling parameter"; rec_call tau 
        | Space_sampling eta -> pl "Space sampling parameter"; rec_call eta
        | Finer s -> pl "Finer scale"; rec_call s 
        | Scale_minimal_dwell s -> pl "Scale of minimal dwell"; rec_call s 
        | Safety  (v,is,ss,cont,stp) -> pl "Safety specification"; rec_call v; rec_call is ; rec_call ss; rec_call cont; rec_call stp
        | Reachability (v,ss,is,ts,m,stp) -> 
	  pl "Reachability specification"; rec_call v; rec_call ss ; rec_call is ; rec_call ts ; rec_call m; ms_succ nb_space ; rec_call stp
        | Space (n,s) -> pl "Space";  rec_call n; box s
        | SpaceMinusSpace (n,s1,s2) -> pl "Space"; rec_call n; box s1; box s2
        | Spacei tg -> ms_wp nb_space ; print_var_type tg
        | Label (md,sc) -> pl "Label"; rec_call md; rec_call sc
        | Plot_2d (aord,mors,mods_colors,scls_colors,xres,yres,lbls,cm) -> 
          pl "Plot";
           ms_succ nb_space ; pl aord;  
           ms_succ nb_space ; pl mors; 
           ms_succ nb_space ; pl "Modes colors: " ;
           List.iter (fun x -> ms_succ (nb_space + 2) ; pl x) mods_colors ;
           ps prf ; ps (repeat_pattern " " (succ nb_space)) ; pl "Scales colors: " ; 
           List.iter (fun x -> ms_succ (nb_space + 2) ; pl x) scls_colors;
           ms_succ nb_space  ; pl "Printing scales: " ;
           rec_call xres; 
           rec_call yres; 
           ms_succ nb_space ; pl "Labels to print: " ;
           box lbls;
           (match cm with
             | None -> ()
             | Some mode -> rec_call mode)
        | Plot_2d3d (aord,mors,mods_colors,scls_colors,xres,yres,lbls,x1,x2,x3max,cm) -> 
          pl "Plot";
           ms_succ nb_space ; pl aord;  
           ms_succ nb_space ; pl mors; 
           ms_succ nb_space ; pl "Modes colors: " ;
           List.iter (fun x -> ms_succ (nb_space + 2) ; pl x) mods_colors ;
           ps prf ; ps (repeat_pattern " " (succ nb_space)) ; pl "Scales colors: " ; 
           List.iter (fun x -> ms_succ (nb_space + 2) ; pl x) scls_colors;
           ms_succ nb_space  ; pl "Printing scales: " ;
           rec_call xres; 
           rec_call yres; 
           ms_succ nb_space ; pl "Labels to print: " ;
           box lbls ;
           rec_call x1 ;
           rec_call x2 ;
           rec_call x3max ;
           (match cm with
             | None -> ()
             | Some mode -> rec_call mode)
        | Plot_2d4d (aord,mors,mods_colors,scls_colors,xres,yres,lbls,x1,x2,x3max,x4max,cm) -> 
          pl "Plot";
           ms_succ nb_space ; pl aord;  
           ms_succ nb_space ; pl mors; 
           ms_succ nb_space ; pl "Modes colors: " ;
           List.iter (fun x -> ms_succ (nb_space + 2) ; pl x) mods_colors ;
           ps prf ; ps (repeat_pattern " " (succ nb_space)) ; pl "Scales colors: " ; 
           List.iter (fun x -> ms_succ (nb_space + 2) ; pl x) scls_colors;
           ms_succ nb_space  ; pl "Printing scales: " ;
           rec_call xres; 
           rec_call yres; 
           ms_succ nb_space ; pl "Labels to print: " ;
           box lbls ;
           rec_call x1 ;
           rec_call x2 ;
           rec_call x3max ; 
           rec_call x4max ;
           (match cm with
             | None -> ()
             | Some mode -> rec_call mode)
        | Plot_arrows (xres,yres,cm) -> 
          pl "Arrows plot"; 
          rec_call xres; rec_call yres ;
          (match cm with
            | None -> ()
            | Some mode -> rec_call mode)
        | Plot_trajectory (state,fin,step,rk4stp,xres,yres,cm,cdn) -> 
          pl "Plot trajectory";
          ms_succ nb_space ; pl "State: " ;
          box state;
          rec_call fin;
          rec_call step;
          rec_call rk4stp;
          rec_call xres; 
          rec_call yres; 
          (match cm with
            | None -> ()
            | Some mode -> rec_call mode);
	  (match cdn with
            | None -> ()
            | Some true -> pl "discrete"
            | Some false -> pl "continuous");
        | Xp e -> pl "Expression : " ; rec_call e
        | Parenthesed_xp e -> pl "Parenthesed expression : " ; rec_call e
        | Binary_op (op, l, r) -> pl (string_of_binop op) ; rec_call l ; rec_call r
        | Unary_op (op, t) -> pl (string_of_unop op) ; rec_call t
        | Id i -> pf "Id: " ; pl i
        | Natural nb -> ps "Natural: "; pl (soi nb)
        | Integer nb ->  ps "Integer: "; pl (soi nb)
        | Real nb -> ps "Real: "; pl (sof nb) 
        | Bool b -> ps "Boolean: "; pl (string_of_bool b)
        | Time -> pl "t"
  in print_rec 0 prefix t
